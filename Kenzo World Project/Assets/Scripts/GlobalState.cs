﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalState : MonoBehaviour
{
    public static GlobalState instance; //creates an instance of this global script
    public bool KeysCollected = false;
    // add any variables to keep track of here
    public Vector2 player_location; //keeps track of player location
    public int milestoneHit = 0; //the most recent trigger the player has hit
    public bool TriggerJustHit = false; //bool to keep track of if a trigger was just hit

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    Vector2 enemy_move_direction = new Vector2(0, 0); //the direction the enemy intends to move
    public Rigidbody2D rb; //handles physics and stuff
    Vector3 final_move_direction; //V3 version of enemy_move_direction
    public int enemyHealth = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHealth <= 0) {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            enemyHealth -= 100;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour
{
    public int playerHealth = 100;
    //player gets inflicted with this damage
    public int enemyDamage = 15;

    public Text healthText;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        healthText.text = playerHealth.ToString();
        if (playerHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("enemyProjectile"))
        {
            Debug.Log("Collided with projectile");
            takeDamage();
            Destroy(collision.gameObject);
        }
    }

    void takeDamage()
    {
        playerHealth -= enemyDamage;
    }
}

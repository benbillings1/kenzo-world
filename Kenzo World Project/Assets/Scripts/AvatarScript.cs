﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarScript : MonoBehaviour{
	public static int keys = 0; //how many keys they have
	Vector2 player_move_direction = new Vector2(0,0); //the direction the player intends to move
	public Rigidbody2D rb; //handles physics and stuff
	Vector3 final_move_direction; //V3 version of player_move_direction
	public GameObject projectile; //template to create all projectiles
	Vector3 last_direction = new Vector3(1,0,0); //direction used to determine which direction to shoot projectile
	//shoot delay variables
	float reload_duration = 1;
	float time_since_last_fire = 0;
	
	// Start is called before the first frame update
	void Start(){
        //rb.isKinematic = true;		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("MessageTrigger"))
        {
            print("the player triggered the message");
            GlobalState.instance.TriggerJustHit = true;
            GlobalState.instance.milestoneHit++;
            //print(GlobalState.instance.milestoneHit);
        }
    }
    // Update is called once per frame
    void FixedUpdate(){
		time_since_last_fire = time_since_last_fire + Time.fixedDeltaTime;
		//determines whether to go up or down
		if (Input.GetKey("up")){
			player_move_direction = new Vector2(player_move_direction.x, 1);
		}else if (Input.GetKey("down")){
			player_move_direction = new Vector2(player_move_direction.x, -1);
		}else{
			player_move_direction = new Vector2(player_move_direction.x, 0);
		}
		
		//determines if to go left or right
		if (Input.GetKey("left")){
			player_move_direction = new Vector2(-1, player_move_direction.y);
		}else if (Input.GetKey("right")){
			player_move_direction = new Vector2(1, player_move_direction.y);	
		}else{
			player_move_direction = new Vector2(0, player_move_direction.y);			
		}
		//actually move character
		final_move_direction = new Vector3(player_move_direction.x, player_move_direction.y, 0);//converts V2 to V3
		if (final_move_direction.x != 0 || final_move_direction.y != 0){
			last_direction = final_move_direction;
		}
		rb.MovePosition(transform.position + (final_move_direction*Time.fixedDeltaTime*3));
		
		//shoot
		if (Input.GetKey("space") && time_since_last_fire > reload_duration){
			time_since_last_fire = 0;
			
			//this area is used to shoot a bullet, to enable once NPCs are ready
			
			GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
			bullet.GetComponent<Rigidbody2D>().AddForce(last_direction * 100);
		}
	}
}

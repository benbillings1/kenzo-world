﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
 
public class EndGameScript : MonoBehaviour{
	public Canvas c;//canvas component of UI
	// Start is called before the first frame update
	void Start(){
		
	}

	void PopUp(){
		c.enabled = true;
		
	}

	// Update is called once per frame
	void Update(){
		if (c.enabled == true && Input.GetKey("R")){
			c.enabled = false;
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MessageNoficationScript : MonoBehaviour
{
    public TextMeshProUGUI ToSay;//what the message will say
    bool waitActive = false;
    //this script is intended to have the message notification appear on scren for the player
    // at specified times during gameplay
    // Start is called before the first frame update
    void Start()
    {

    }
    IEnumerator Wait()//enumerator for the message wait duration
    {
        waitActive = true;
        yield return new WaitForSeconds(3.0f);
        SetActiveAllChildren(this.transform, false);//sets message to inactive until next checkpoint
        waitActive = false;
    }

    private void SetActiveAllChildren(Transform transform, bool value)
    {
        foreach(Transform child in transform){
            child.gameObject.SetActive(value);
            SetActiveAllChildren(child, value);
        }
    }
    void activateNotication()//function to activate the notification for a specified amount of time on the player screen
    { 
        SetActiveAllChildren(this.transform, true);//sets the message active
        if (!waitActive)
        {
            StartCoroutine(Wait());//does a wait to have notifiaction appear
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        if (GlobalState.instance.TriggerJustHit) {
            print("the message script read the trigger hit");
            GlobalState.instance.TriggerJustHit = false;
            if (GlobalState.instance.milestoneHit == 1)
            {
                //print("this text should pop up after this");
                ToSay.SetText("Hey, what's up?");
            }
            if (GlobalState.instance.milestoneHit == 2)
            {
                ToSay.SetText("Hellloooo?");
            }
            if (GlobalState.instance.milestoneHit == 3)
            {
                ToSay.SetText("God I hope you aren't on that stupid game again...");
            }
            if (GlobalState.instance.milestoneHit == 4)
            {
                ToSay.SetText("Well, I can say I tried");
            }
            if (GlobalState.instance.milestoneHit == 5)
            {
                ToSay.SetText("Seriously, what did i do to you?");
            }
            activateNotication();

        }
    }
}

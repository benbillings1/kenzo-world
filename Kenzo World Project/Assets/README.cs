﻿/*
	Hello there!
	
	Here is some stuff you need to know to work on this game.
	
	WHAT IS THIS GAME?:
		-It is a game based off of the song World by Kenzo, where a woman escapes a boring business dinner party to be her true self
		-We took this theme to be of temporarily avoiding responsibilities
		-Our idea spawned from that is you are a business person who is a videogamer at heart.
		-You try to complete a small MMORPG parody game quest (namely slay a dragon after finding all 4 keys to unlock the fight)
		-But as you try to complete this quest you keep getting emails from business, you have to pause the in-game game at key times to quickly respond to emails.
		-If too many emails build up you lose, also if your in-game character dies you lose. 
		-To complete emails we had the idea that you would type out a pre-written prompt like in typer shark. 
		-A PDF of the rough GDD has been included (check under "Assets" folder in file explorer).
		
	FORMAT:
		-Variables: all lowercase with underscores separating words
		-Classes: all capital with no spaces
		-Functions: all capital with no spaces
		-Commenting: comment any variables assigned more than once 
		-Assets: lowercase no spaces
		-Folders: uppercase no spaces
		-
	CURRENT STATE AS OF 9/9/2019:
		-No assets are final
		-Boss battle not added
		-Avatar can move around (some projectile launching code is commented out)
		-Rough tilemap set up (feel free to redesign)
		-Key asset collection completed
		-None of the email centered gameplay has been started
		
	OUR BLESSING:
		You are now in charge of this game, if you find it necessary to pivot or adjust pre-existing stuff we are fine with that. 
		Good luck!
*/
